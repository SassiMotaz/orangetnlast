from django import forms
from django.contrib.auth.forms import UserCreationForm , UserChangeForm
from .models import Customer
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse_lazy
from django.contrib.auth import get_user_model


class CustomerChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Customer
        fields = ['username','first_name','last_name','email']
        labels = {
            'username': 'Nom d\'utilisateur',
            'first_name': 'Prénom',
            'last_name': 'Nom',
            'email': 'Email',
        }

        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control' }),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control' }),
            'last_name': forms.TextInput(attrs={'class': 'form-control' }),
        }

class CustomerPasswordChangeForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['old_password'].label = "Ancien mot de passe"
        self.fields['new_password1'].label = "Nouveau mot de passe"
        self.fields['new_password2'].label = "Confirmer le nouveau mot de passe"
        self.fields['old_password'].widget.attrs['class'] = 'form-control'
        self.fields['new_password1'].widget.attrs['class'] = 'form-control'
        self.fields['new_password2'].widget.attrs['class'] = 'form-control'

    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')
        if not self.user.check_password(old_password):
            raise forms.ValidationError("Ancien mot de passe incorrect")
        return old_password
    

class CustomerCreationForm(UserCreationForm):
    class Meta:
        model = Customer
        fields = ['username','first_name','last_name','email','password1','password2','groups']
        labels = {
            'username': 'Nom d\'utilisateur',
            'first_name': 'Prénom',
            'last_name': 'Nom',
            'email': 'Email',
            'password1': 'Mot de passe',
            'password2': 'Confirmer le mot de passe',
            'groups': 'Groupes',
        }

        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control' }),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control' }),
            'last_name': forms.TextInput(attrs={'class': 'form-control' }),
            'password1': forms.PasswordInput(attrs={'class': 'form-control' }),
            'password2': forms.PasswordInput(attrs={'class': 'form-control' }),
            'groups': forms.CheckboxSelectMultiple(attrs={'class': 'form-check-input' }),
        }

        error_messages = {
            'username': {
                'unique': "Ce nom d'utilisateur existe déjà",
            },
            'password_mismatch': 'Les deux mots de passe ne correspondent pas',
            'password2': {
                'required': 'Veuillez confirmer votre mot de passe',
            },
            'password1': {
                'required': 'Veuillez saisir un mot de passe',
                'min_length': 'Le mot de passe doit contenir au moins 8 caractères',
                'max_length': 'Le mot de passe doit contenir au plus 20 caractères',

            },
            'email': {
                'unique': "Cet email existe déjà",
                'required': 'Veuillez saisir un email',
                'invalid': 'Veuillez saisir un email valide',
                'max_length': 'L\'email doit contenir au plus 254 caractères',
                'min_length': 'L\'email doit contenir au moins 3 caractères',
            },
            'groups': {
                'required': 'Veuillez choisir un groupe',
            },
        }
        

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if get_user_model().objects.filter(username=username).exists():
            raise forms.ValidationError("Ce nom d'utilisateur existe déjà")
        return username