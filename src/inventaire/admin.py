from django.contrib import admin

# Register your models here.

from .models import Server , StorageRack , IncidentServer , IncidentStorageRack 

admin.site.register(Server)

admin.site.register(StorageRack)

admin.site.register(IncidentServer)

admin.site.register(IncidentStorageRack)

