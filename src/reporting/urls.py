from django.urls import path
from .import views
from .views import ServerGraphPDFView , indexreport

app_name = 'reporting'

urlpatterns = [
    path('inventory-report/', ServerGraphPDFView.as_view(), name='inventory_report'),
    path('email-report/', views.email_report, name='email_report'),
    path('index/', indexreport, name='index'),
]
