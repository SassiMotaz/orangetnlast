# Utilisez une image de base avec Python préinstallé
FROM python:3.9

# Installez Nginx
RUN apt-get update && apt-get install -y nginx

# Définissez le répertoire de travail de l'application dans le conteneur
WORKDIR /app/src

# Copiez le fichier requirements.txt dans le conteneur
COPY requirements.txt .

# Installez les dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Copiez le reste des fichiers de l'application dans le conteneur
COPY src .

# Copiez le fichier de configuration Nginx dans le conteneur
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Exposez le port que votre application Django utilise
EXPOSE 8000

# Exécutez Nginx et votre application Django
CMD service nginx start && python src/manage.py runserver 0.0.0.0:8000
