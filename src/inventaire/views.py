import os
from django.contrib.auth.decorators import user_passes_test
from django.db.models import Q
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from matplotlib import pyplot as plt
from .models import  Server , StorageRack , IncidentServer , IncidentStorageRack
from django.urls import reverse_lazy
from django.http import HttpResponse
from openpyxl import load_workbook
import xlsxwriter 
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .forms import ServerForm , StorageRackForm , IncidentServerForm , IncidentStorageRackForm
from django.shortcuts import render
from django.db.models import Count
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from  prometheus_client import Counter, MetricsHandler
from django.db.models import Count
from io import BytesIO
import base64
import matplotlib.pyplot as plt
from .models import Server
from .forms import ServerChartForm
import plotly.graph_objs as go


# Create server views here.
def index(request):
    return render(request, 'indexinven.html')

def indexserver(request):
    return render(request, 'server/indexserver.html')

def indexstoragerack(request):
    return render(request, 'storagerack/indexstoragerack.html')

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class ServerView(ListView):
    model = Server
    template_name = 'server/server_list.html'
    context_object_name = 'server_list'
    ordering = ['-acquisition_date']
    paginate_by = 5

    def get_queryset(self):
        queryset = super().get_queryset()
        query = self.request.GET.get('q')
        if query:
            queryset = queryset.filter(
                Q(serial_number__icontains=query) |
                Q(brand__icontains=query) |
                Q(site__icontains=query) |
                Q(project__icontains=query) |
                Q(operating_system__icontains=query)
            )
        return queryset
    
@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class ServerDetailView(DetailView):
    model = Server
    template_name = 'server/server_detail.html'
    context_object_name = 'server_detail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['incidentserver_list'] = IncidentServer.objects.filter(server=self.object)
        return context
    
@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class ServerCreateView(CreateView):
    model = Server
    template_name = 'server/server_create.html'
    form_class = ServerForm
    success_url = reverse_lazy('inventaire:server')

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class ServerUpdateView(UpdateView):
    model = Server
    template_name = 'server/server_update.html'
    form_class = ServerForm
    success_url = reverse_lazy('inventaire:gestionserver')

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class ServerDeleteView(DeleteView):
    model = Server
    template_name = 'server/server_delete.html'
    context_object_name = 'server_detail'
    success_url = reverse_lazy('inventaire:server')

def export_servers_to_excel(request):
    # Retrieve the server data to export (you can modify this to suit your needs)
    servers = Server.objects.all()

    # Create a new Excel workbook and sheet
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="servers.xlsx"'
    workbook = xlsxwriter.Workbook(response, {'in_memory': True})
    worksheet = workbook.add_worksheet()
    # Define the column headers
    headers = ['Serial number', 'Brand', 'Site', 'Support', 'Project', 'Acquisition date', 'MEP date', 'Operating system', 'RAM', 'Disk', 'CPU', 'Power']
    # Write the column headers to the first row of the sheet
    row = 0
    col = 0
    for header in headers:
        worksheet.write(row, col, header)
        col += 1
    # Write the server data to the sheet
    row = 1
    for server in servers:
        worksheet.write(row, 0, server.serial_number)
        worksheet.write(row, 1, server.brand)
        worksheet.write(row, 2, server.site)
        worksheet.write(row, 3, server.support)
        worksheet.write(row, 4, server.project)
        worksheet.write(row, 5, server.acquisition_date.strftime('%Y-%m-%d'))
        worksheet.write(row, 6, server.mep_date.strftime('%Y-%m-%d'))
        worksheet.write(row, 7, server.operating_system)
        worksheet.write(row, 8, server.ram)
        worksheet.write(row, 9, server.disk)
        worksheet.write(row, 10, server.cpu)
        worksheet.write(row, 11, server.power)
        row += 1

    # Close the workbook and return the response
    workbook.close()
    return response

def import_servers_from_excel(request):
    # Check if a file was uploaded
    if 'file' not in request.FILES:
        return render(request, 'server/import.html', {'error': 'No file selected'})

    # Get the uploaded file and load it into an openpyxl workbook object
    file = request.FILES['file']
    workbook = load_workbook(filename=file, read_only=True)

    # Get the active worksheet in the workbook
    worksheet = workbook.active

    # Loop over each row in the worksheet, skipping the header row
    for row in worksheet.iter_rows(min_row=2, values_only=True):
        # Convert datetime objects to strings
        acquisition_date_str = row[5]
        mep_date_str = row[6]

        # Create a new Server object using the data from the row
        server = Server(
            serial_number=row[0],
            brand=row[1],
            site=row[2],
            support=row[3],
            project=row[4],
            acquisition_date=acquisition_date_str,
            mep_date=mep_date_str,
            operating_system=row[7],
            ram=row[8],
            disk=row[9],
            cpu=row[10],
            power=row[11],
        )
        # Save the new Server object to the database
        server.save()
               

    # Redirect back to the import page with a success message
    return redirect('inventaire:server')

def previous_page(request):
    return redirect(request, "server/chary")

def chart_form(request):
    fields = ['serial_number', 'ip_address', 'brand', 'site', 'support', 
              'project', 'acquisition_date', 'mep_date', 'operating_system',
              'ram', 'disk', 'cpu', 'power', 'document']
    return render(request, 'server/chart_form.html', {'fields': fields})

def generate_chart(request):
    if request.method == 'POST':
        x_field = request.POST.get('x_field') # récupère la valeur de la clé 'x_field'
        # autres opérations pour générer le graphique
    else:
        # si la requête n'est pas POST, redirige l'utilisateur vers la page précédente
        return redirect(request.META.get('HTTP_REFERER'))   
    # Récupération des données
    x_field = request.POST['x_field']
    y_field = request.POST['y_field']
    servers = Server.objects.all()

    # Création des données pour le graphique
    x_data = [getattr(server, x_field) for server in servers]
    y_data = [getattr(server, y_field) for server in servers]
     
    # Création du graphique
    fig = go.Figure(data=go.Bar(x=x_data, y=y_data, marker_color='orangered'))
         
    # Personnalisation du graphique
    fig.update_layout(
        xaxis_title=x_field,
        yaxis_title=y_field,
        title=f"{x_field} par rappor à {y_field}"
    )

    # Rendu de la page
    chart = fig.to_html(full_html=False)

    return render(request, 'server/chart.html', {'chart': chart})
# Create storage rack views here.

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class StorageRackView(ListView):
    model = StorageRack
    template_name = 'storagerack/storage_rack_list.html'
    context_object_name = 'storage_rack_list'
    ordering = ['-acquisition_date']
    paginate_by = 5

    def get_queryset(self):
        queryset = super().get_queryset()
        query = self.request.GET.get('q')
        if query:
            queryset = queryset.filter(
                Q(serial_number__icontains=query) |
                Q(brand__icontains=query) |
                Q(site__icontains=query) |
                Q(project__icontains=query)
            )

        return queryset

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch') 
class StorageRackDetailView(DetailView):
    model = StorageRack
    template_name = 'storagerack/storage_rack_detail.html'
    context_object_name = 'storage_rack_detail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['incident_list'] = IncidentStorageRack.objects.filter(storage_rack=self.object)
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class StorageRackCreateView(CreateView):
    model = StorageRack
    template_name = 'storagerack/storage_rack_create.html'
    form_class = StorageRackForm
    success_url = reverse_lazy('inventaire:storagerack')

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class StorageRackUpdateView(UpdateView):
    model = StorageRack
    template_name = 'storagerack/storage_rack_update.html'
    form_class = StorageRackForm
    success_url = reverse_lazy('inventaire:storagerack')

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['SMC','Admin']).exists() or u.is_superuser), name='dispatch')
class StorageRackDeleteView(DeleteView):
    model = StorageRack
    template_name = 'storagerack/storage_rack_delete.html'
    context_object_name = 'storage_rack_detail'
    success_url = reverse_lazy('inventaire:storagerack')

def export_storageracks_to_excel(request):
# Retrieve the server data to export (you can modify this to suit your needs)
    storageracks = StorageRack.objects.all()

    # Create a new Excel workbook and sheet
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="storageracks.xlsx"'
    workbook = xlsxwriter.Workbook(response, {'in_memory': True})
    worksheet = workbook.add_worksheet()
    # Define the column headers
    headers = ['Serial number', 'Brand', 'Site' ,'Support','Project', 'Acquisition date', 'MEP date','Model','Type', 'Capacity', 'Power']
    # Write the column headers to the first row of the sheet
    row = 0
    col = 0
    for header in headers:
        worksheet.write(row, col, header)
        col += 1
    # Write the server data to the sheet
    row = 1
    for storagerack in storageracks:
        worksheet.write(row, 0, storagerack.serial_number)
        worksheet.write(row, 1, storagerack.brand)
        worksheet.write(row, 2, storagerack.site)
        worksheet.write(row, 3, storagerack.support)
        worksheet.write(row, 4, storagerack.project)
        worksheet.write(row, 5, storagerack.acquisition_date.strftime('%Y-%m-%d'))
        worksheet.write(row, 6, storagerack.mep_date.strftime('%Y-%m-%d'))
        worksheet.write(row, 7, storagerack.model)
        worksheet.write(row, 8, storagerack.type_of_storage)
        worksheet.write(row, 9, storagerack.capacity)
        worksheet.write(row, 10, storagerack.power)
        
        row += 1

    # Close the workbook and return the response
    workbook.close()
    return response

def import_storageracks_from_excel(request):
    # Check if a file was uploaded
    if 'file' not in request.FILES:
        return render(request, 'storagerack/import.html', {'error': 'No file selected'})

    # Get the uploaded file and load it into an openpyxl workbook object
    file = request.FILES['file']
    workbook = load_workbook(filename=file, read_only=True)

    # Get the active worksheet in the workbook
    worksheet = workbook.active

    # Loop over each row in the worksheet, skipping the header row
    for row in worksheet.iter_rows(min_row=2, values_only=True):
        # Convert datetime objects to strings
        acquisition_date_str = row[5]
        mep_date_str = row[6]

        # Create a new Server object using the data from the row
        storagerack = StorageRack(
            serial_number=row[0],
            brand=row[1],
            site=row[2],
            support=row[3],
            project=row[4],
            acquisition_date=acquisition_date_str,
            mep_date=mep_date_str,
            model=row[7],
            type_of_storage=row[8],
            capacity=row[9],
            power=row[10],
        )
        # Save the new Server object to the database
        storagerack.save()

    # Redirect back to the import page with a success message
    return render(request, 'storagerack/import.html', {'success': 'StorageRacks imported successfully'})

def storagerack_form(request):
    fields = ['serial_number', 'ip_address', 'brand', 'site', 'support', 
              'project', 'acquisition_date', 'mep_date', 'model',
              'type_of_storage', 'capacity',  'power', 'document']
    return render(request, 'storagerack/chart_form.html', {'fields': fields})

def storagerack_chart(request):
    if request.method == 'POST':
        x_field = request.POST.get('x_field') # récupère la valeur de la clé 'x_field'
        # autres opérations pour générer le graphique
    else:
        # si la requête n'est pas POST, redirige l'utilisateur vers la page précédente
        return redirect(request.META.get('HTTP_REFERER'))   
    # Récupération des données
    x_field = request.POST['x_field']
    y_field = request.POST['y_field']
    racks = StorageRack.objects.all()

    # Création des données pour le graphique
    x_data = [getattr(server, x_field) for server in racks]
    y_data = [getattr(server, y_field) for server in racks]

    # Création du graphique
    fig = go.Figure(data=go.Bar(x=x_data, y=y_data, marker_color='orangered'))

    # Personnalisation du graphique
    fig.update_layout(
        xaxis_title=x_field,
        yaxis_title=y_field,
        title=f"{x_field} par rappor à {y_field}"
    )

    # Rendu de la page
    chart = fig.to_html(full_html=False)

    return render(request, 'storagerack/chart.html', {'chart': chart})
# Create Incident views for server

def indexincident(request):
    return render(request, 'incident/index.html')


class IncidentServerListView(ListView):
    model = IncidentServer
    template_name = 'incident/incident_server.html'
    context_object_name = 'incident_server_list'
    paginate_by = 10
    ordering = ['id']

class IncidentServerDetailView(DetailView):
    model = IncidentServer
    template_name = 'incident/incident_server_detail.html'
    context_object_name = 'incident_server_detail'

class IncidentServerCreateView(CreateView):
    model = IncidentServer
    template_name = 'incident/incident_server_create.html'
    form_class = IncidentServerForm
    success_url = reverse_lazy('inventaire:index')

class IncidentServerUpdateView(UpdateView):
    model = IncidentServer
    template_name = 'incident/incident_server_update.html'
    form_class = IncidentServerForm
    success_url = reverse_lazy('inventaire:index')

class IncidentServerDeleteView(DeleteView):
    model = IncidentServer
    template_name = 'incident/incident_server_delete.html'
    context_object_name = 'incident_server_detail'
    success_url = reverse_lazy('inventaire:index')

# Create Incident views for storgerack

class IncidentStorageRackListView(ListView):
    model = IncidentStorageRack
    template_name = 'incident/incident_storagerack.html'
    context_object_name = 'incident_storagerack_list'
    paginate_by = 10

class IncidentStorageRackDetailView(DetailView):
    model = IncidentStorageRack
    template_name = 'incident/incident_storagerack_detail.html'
    context_object_name = 'incident_storagerack_detail'

class IncidentStorageRackCreateView(CreateView):
    model = IncidentStorageRack
    template_name = 'incident/incident_storagerack_create.html'
    form_class = IncidentStorageRackForm
    success_url = reverse_lazy('inventaire:index')

class IncidentStorageRackUpdateView(UpdateView):
    model = IncidentStorageRack
    template_name = 'incident/incident_storagerack_update.html'
    form_class = IncidentStorageRackForm
    success_url = reverse_lazy('inventaire:index')

class IncidentStorageRackDeleteView(DeleteView):
    model = IncidentStorageRack
    template_name = 'incident/incident_storagerack_delete.html'
    context_object_name = 'incident_storagerack_detail'
    success_url = reverse_lazy('inventaire:index')

