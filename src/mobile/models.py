from django.db import models

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    mobiles = models.ManyToManyField('Mobile', related_name='categories')

    def __str__(self):
        return self.title


class Mobile(models.Model):
    title = models.CharField(max_length=100)
    price = models.FloatField(default=0)
    rom = models.IntegerField(default=0)
    ram = models.IntegerField(default=0)
    cpu = models.CharField(max_length=100 , null=True, blank=True)
    taille = models.FloatField(default=0)
    resolution = models.CharField(max_length=100, null=True, blank=True)
    profndeur = models.FloatField(default=0)
    Rafra = models.FloatField(default=0)
    system = models.CharField(max_length=100, null=True, blank=True)
    battery = models.IntegerField(default=0)
    type = models.CharField(max_length=100, null=True, blank=True)
    camera_front = models.IntegerField(default=0)
    camera_rear = models.IntegerField(default=0)
    video = models.CharField(max_length=100, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    
class MobileImage(models.Model):
    mobile = models.ForeignKey(Mobile, on_delete=models.CASCADE , related_name='images', null=True)
    image = models.ImageField(upload_to='mobile_images/')

    def __str__(self):
        return self.mobile.title
