"""ECommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from accounts.views import  index , MyLoginView  , update , profile , CustomerPasswordChangeView , CustomerListView , detail , CustomerRegistrationView , delete
from django.contrib.auth.views import LogoutView 
from .views import CustomerPasswordChangeView , login_log , MyLogoutView , parameter
app_name="accounts"

urlpatterns = [
    path('',index , name="index"),
    path('parameter/',parameter , name="parameter"),
    path('login/',MyLoginView.as_view() ,name="login"),
    path('logout/', MyLogoutView.as_view(), name='logout'),
    path('register/',CustomerRegistrationView.as_view() ,name="register"),
    path('profile/',profile ,name="profile"),
    path('detail/<int:pk>/',detail ,name="detail"),
    path('list/',CustomerListView.as_view() ,name="list"),
    path('update/<int:pk>/',update ,name="update"),
    path('password/<int:pk>/',CustomerPasswordChangeView.as_view() ,name="password"),
    path('delete/<int:pk>/',delete ,name="delete"),
    path('login_log/<int:pk>/',login_log ,name="login_log"),
]
