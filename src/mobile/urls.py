from django.urls import path

from .models import Category, Mobile, MobileImage
from django.forms import ModelForm
from mobile.views import CategoryListView , CategoryDetailView , CategoryCreateView , CategoryUpdateView , CategoryDeleteView , MobileCategoryListView
from mobile.views import MobileListView , MobileCreateView , MobileUpdateView , MobileDeleteView , MobileDetailView
from mobile.views import index
app_name = 'mobile'

urlpatterns = [
    path('', index, name='index'),
    # Category URLs
    path('category', CategoryListView.as_view(), name='home'),    
    path('category/<int:pk>/', CategoryDetailView.as_view(), name='category_detail'),
    path('category/create/', CategoryCreateView.as_view(), name='category_create'),
    path('category/update/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),
    path('category/delete/<int:pk>/', CategoryDeleteView.as_view(), name='category_delete'),
    # Mobile URLs
    path('mobile/list/', MobileListView.as_view(), name='mobile_list'),
    path('mobile/create/', MobileCreateView.as_view(), name='mobile_create'),
    path('mobile/update/<int:pk>/', MobileUpdateView.as_view(), name='mobile_update'),
    path('mobile/delete/<int:pk>/', MobileDeleteView.as_view(), name='mobile_delete'),
    path('mobile/<int:pk>/', MobileDetailView.as_view(), name='mobile_detail'),
    path('mobile/<int:pk>/category/', MobileCategoryListView.as_view(), name='mobile_category_list'),
]