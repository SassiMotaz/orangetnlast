"""ECommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from services.views import HomeView  , ServiceCreateView , ServiceUpdateView , ServiceDeleteView , ServiceDetailView , index
from services.views import CategoryServiceListView , CategoryServiceDetailView , CategoryServiceCreateView , CategoryServiceUpdateView , CategoryServiceDeleteView , ServicesCategoryListView
app_name="services"

urlpatterns = [
    path('', index, name='index'),
    path('list/', HomeView.as_view(), name='home'),
    path('create/', ServiceCreateView.as_view() , name="create"),
    path('update/<int:pk>', ServiceUpdateView.as_view() , name="upated"),
    path('delete/<int:pk>', ServiceDeleteView.as_view() , name="delete"),
    path('detail/<int:pk>', ServiceDetailView.as_view() , name="detail"),
    path('category/', CategoryServiceListView.as_view() , name="category"),
    path('category/<int:pk>', CategoryServiceDetailView.as_view() , name="category_detail"),
    path('category/create/', CategoryServiceCreateView.as_view() , name="category_create"),
    path('category/update/<int:pk>', CategoryServiceUpdateView.as_view() , name="category_update"),
    path('category/delete/<int:pk>', CategoryServiceDeleteView.as_view() , name="category_delete"),
    path('category/<int:pk>/services', ServicesCategoryListView.as_view() , name="category_services"),


]
