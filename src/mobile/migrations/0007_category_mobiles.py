# Generated by Django 4.2a1 on 2023-05-09 10:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mobile', '0006_alter_mobile_rafra_alter_mobile_price_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='mobiles',
            field=models.ManyToManyField(related_name='categories', to='mobile.mobile'),
        ),
    ]
