from django import forms

from .models import Server , StorageRack , IncidentServer , IncidentStorageRack

# form for the server

class DateInput(forms.DateInput):
    input_type = 'date'

class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        fields = ('serial_number','brand','site','support','project','ip_address','acquisition_date','mep_date','operating_system','ram','disk','cpu','power','document')
        labels = {
            'serial_number': 'Numéro de série',
            'brand': 'Marque',
            'site': 'Site',
            'support': 'Support',
            'project': 'Projet',
            'ip_address': 'Adresse IP',
            'acquisition_date': 'Date d\'acquisition',
            'mep_date': 'Date de mise en production',
            'operating_system': 'Système d\'exploitation',
            'ram': 'Mémoire vive',
            'disk': 'Disque dur',
            'cpu': 'Processeur',
            'power': 'Puissance',
            'document': 'Document',
        }
        widgets = {
            'serial_number': forms.TextInput(attrs={'class': 'form-control'}),
            'brand': forms.TextInput(attrs={'class': 'form-control'}),
            'site': forms.TextInput(attrs={'class': 'form-control'}),
            'support': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'project': forms.TextInput(attrs={'class': 'form-control'}),
            'ip_address': forms.TextInput(attrs={'class': 'form-control'}),
            'acquisition_date': DateInput(attrs={'class': 'form-control'}),
            'mep_date': DateInput(attrs={'class': 'form-control'}),
            'operating_system': forms.TextInput(attrs={'class': 'form-control'}),
            'ram': forms.NumberInput(attrs={'class': 'form-control'}),
            'disk': forms.NumberInput(attrs={'class': 'form-control'}),
            'cpu': forms.TextInput(attrs={'class': 'form-control'}),
            'power': forms.NumberInput(attrs={'class': 'form-control'}),
            'document': forms.TextInput(attrs={'class': 'form-control'}),
        }

# form for the storage rack

class StorageRackForm(forms.ModelForm):
    class Meta:
        model = StorageRack
        fields = ('serial_number','brand','site','support','project','acquisition_date','mep_date','model','type_of_storage','capacity','power','document')
        labels = {
            'serial_number': 'Numéro de série',
            'brand': 'Marque',
            'site': 'Site',
            'support': 'Support',
            'project': 'Projet',
            'acquisition_date': 'Date d\'acquisition',
            'mep_date': 'Date de mise en production',
            'model': 'Modèle',
            'type_of_storage': 'Type de stockage',
            'capacity': 'Capacité',
            'power': 'Puissance de consommation',
            'document': 'Document',
        }
        widgets = {
            'serial_number': forms.TextInput(attrs={'class': 'form-control'}),
            'brand': forms.TextInput(attrs={'class': 'form-control'}),
            'site': forms.TextInput(attrs={'class': 'form-control'}),
            'support': forms.CheckboxInput(attrs={'class': 'form-check-input'}),
            'project': forms.TextInput(attrs={'class': 'form-control'}),
            'ip_address': forms.TextInput(attrs={'class': 'form-control'}),
            'acquisition_date': DateInput(attrs={'class': 'form-control'}),
            'mep_date': DateInput(attrs={'class': 'form-control'}),
            'model': forms.TextInput(attrs={'class': 'form-control'}),
            'type_of_storage': forms.TextInput(attrs={'class': 'form-control'}),
            'capacity': forms.NumberInput(attrs={'class': 'form-control'}),
            'power': forms.NumberInput(attrs={'class': 'form-control'}),
            'document': forms.TextInput(attrs={'class': 'form-control'}),
        }
    
# form for the incident server

class IncidentServerForm(forms.ModelForm):
    class Meta:
        model = IncidentServer
        fields = ('type_of_incident','description','date','solution','server')
        labels = {
            'type_of_incident': 'Type d\'incident',
            'description': 'Description de l\'incident',
            'date': 'Date de l\'incident',
            'solution': 'Solution apportée',
            'server': 'Serveur concerné',
        }
        widgets = {
            'type_of_incident': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'date': DateInput(attrs={'class': 'form-control'}),
            'solution': forms.Textarea(attrs={'class': 'form-control'}),
            'server': forms.Select(attrs={'class': 'form-select mt-2'}),
        }
        
# form for the incident storage rack

class IncidentStorageRackForm(forms.ModelForm):
    class Meta:
        model = IncidentStorageRack
        fields = ('type_of_incident','description','date','solution','storage_rack')
        labels = {
            'type_of_incident': 'Type d\'incident',
            'description': 'Description',
            'date': 'Date',
            'solution': 'Solution',
            'storage_rack': 'Baie de stockage',
        }
        widgets = {
            'type_of_incident': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'date': DateInput(attrs={'class': 'form-control'}),
            'solution': forms.Textarea(attrs={'class': 'form-control'}),
            'storage_rack': forms.Select(attrs={'class': 'form-control'}),
        }
#form pour les chartts graphique de la liste des serveurs 
class ServerChartForm(forms.Form):
    field1 = forms.ChoiceField(choices=[(f.name, f.verbose_name) for f in Server._meta.fields])
    field2 = forms.ChoiceField(choices=[(f.name, f.verbose_name) for f in Server._meta.fields])