from django.contrib.auth import login
from django.http import request
from django.shortcuts import redirect, render
from django.contrib.auth.views import LoginView , PasswordChangeView , LogoutView
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.decorators import permission_required , login_required
from .form import CustomerChangeForm , CustomerPasswordChangeForm
from django.urls import reverse_lazy
from django.views.generic import ListView
from .models import Customer
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.views.generic.edit import CreateView , DeleteView
from .form import CustomerCreationForm
from .models import LoginLog
from django.utils import timezone
import datetime



def index (request):
  return render(request, 'accounts/index.html')

def parameter (request):
  return render(request, 'accounts/parameter.html')

class MyLoginView(LoginView):
    redirect_authenticated_user = True
    template_name = 'accounts/login.html'
    
    def get_success_url(self):
        return reverse_lazy('home')
    
    def form_invalid(self, form):
        messages.error(self.request,'Nom d\'utilisateur ou mot de passe incorrect')
        return self.render_to_response(self.get_context_data(form=form))
    def form_valid(self, form):
        response = super().form_valid(form)
        login_log = LoginLog(user=self.request.user, login_time=timezone.now())
        login_log.save()
        return response


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class CustomerRegistrationView(CreateView):
    model = Customer
    form_class = CustomerCreationForm
    template_name = 'accounts/register.html'
    success_url = reverse_lazy('accounts:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Register'
        return context

    def form_valid(self, form):
        user = form.save()
        messages.success(self.request, 'Registration successful!')
        return redirect('accounts:detail',pk=user.id)
 
@login_required
def update(request,pk):
    customer = Customer.objects.get(id=pk)
    groups = Customer.objects.get(id=pk).groups.all()
    if request.method == 'POST':
        form = CustomerChangeForm(request.POST, instance=customer)
        if form.is_valid():
            form.save()
            messages.success(request, 'Profile updated successfully!')
            return redirect('accounts:detail',pk=pk)
    else:
        form = CustomerChangeForm(instance=customer)
    return render(request, 'accounts/update.html',{'form':form,'customer':customer,'groups':groups})

class CustomerPasswordChangeView(PasswordChangeView):
    template_name = 'accounts/password1.html'
    success_url = 'accounts:index'
    form_class = CustomerPasswordChangeForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Change Password'
        return context
    
    def form_valid(self, form):
        messages.success(self.request, 'Password changed successfully!')
        super().form_valid(form)
        return redirect('accounts:detail',pk=self.request.user.id)
    
@login_required
def profile(request):
    return render(request, 'accounts/profile.html')

def delete(request,pk):
    customer = Customer.objects.get(id=pk)
    if request.method == 'POST':
        customer.delete()
        messages.success(request, 'Profile deleted successfully!')
        return redirect('accounts:list')
    return render(request, 'accounts/delete.html',{'customer':customer})

@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class CustomerListView(ListView):
    model = Customer
    template_name = 'accounts/customer_list.html'
    context_object_name = 'customers'
    paginate_by = 10

    def get_queryset(self):
        return Customer.objects.exclude(id=self.request.user.id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Customer List'
        return context
    
@permission_required('auth.group_admin')
def detail(request,pk):
    customer = Customer.objects.get(id=pk)
    login = LoginLog.objects.filter(user=customer)
    return render(request, 'accounts/details.html',{'customer':customer , 'login':login})

@permission_required('auth.group_admin')
def login_log(request,pk):
    customer = Customer.objects.get(id=pk)
    login = LoginLog.objects.filter(user=customer)
    return render(request, 'accounts/login_log.html',{'customer':customer , 'login':login})


class MyLogoutView(LogoutView):
    template_name = 'accounts/logout.html'
    next_page = reverse_lazy('accounts:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Logout'
        return context
    
    def dispatch(self, request, *args, **kwargs):
        login_log = LoginLog.objects.filter(user=self.request.user).last()
        login_log.logout_time = timezone.now()
        login_log.save()
        return super().dispatch(request, *args, **kwargs)
    
