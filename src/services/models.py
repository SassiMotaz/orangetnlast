from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.urls import reverse
from django import forms
from mobile.models import Mobile

# Create your models here.

class CategoryService(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    services = models.ManyToManyField('Service', blank=True, related_name='services')

    def __str__(self):
        return self.title   

class Service(models.Model):
    title = models.CharField(max_length=200, verbose_name="Title")
    subtitle = models.CharField(max_length=200, verbose_name="Subtitle" , blank=True, null=True)
    pubished = models.BooleanField(verbose_name="Published", default=False)
    content = models.TextField(verbose_name="Content")
    image = models.ImageField(verbose_name="Image", upload_to="services/" , blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Created")
    updated = models.DateTimeField(auto_now=True, verbose_name="Updated")
    Appels = models.IntegerField(verbose_name="Appels", default=0)
    SMS = models.IntegerField(verbose_name="SMS", default=0)
    Prix = models.FloatField(verbose_name="Prix", default=0)
    mobiles = models.ManyToManyField(Mobile, blank=True, verbose_name="Mobiles")
    category = models.ForeignKey(CategoryService, on_delete=models.CASCADE, verbose_name="Category")

    class Meta:
        verbose_name = "service"
        verbose_name_plural = "services"
        ordering = ["-created"]

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.subtitle = slugify(self.title)
        self.created = timezone.now()
        super(Service, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("services:home")
