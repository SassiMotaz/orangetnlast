from django.db import models
from django.conf import settings
from django.utils import timezone

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter

# Create your models here.

class Server(models.Model):
    serial_number = models.CharField(max_length=50 , primary_key=True )
    ip_address = models.CharField(max_length=50 , null=True)
    brand = models.CharField(max_length=50)
    site = models.CharField(max_length=50)
    support = models.BooleanField(default=False)
    project = models.CharField(max_length=50)
    acquisition_date = models.DateField()
    mep_date = models.DateField()
    operating_system = models.CharField(max_length=50)
    ram = models.CharField(max_length=50)
    disk = models.CharField(max_length=50)
    cpu = models.CharField(max_length=50)
    power = models.CharField(max_length=50)
    document = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.serial_number
                      
class StorageRack(models.Model):
    serial_number = models.CharField(max_length=50 , primary_key=True)
    brand = models.CharField(max_length=50)
    site = models.CharField(max_length=50)
    support = models.BooleanField(default=False)
    project = models.CharField(max_length=50)
    acquisition_date = models.DateField()
    mep_date = models.DateField()
    model = models.CharField(max_length=50)
    type_of_storage = models.CharField(max_length=50)
    capacity = models.CharField(max_length=50)
    power = models.CharField(max_length=50)
    document = models.CharField(max_length=50 , null=True)
    

    def __str__(self):
        return self.serial_number
    
class IncidentServer(models.Model):
    type_of_incident = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    date = models.DateField()
    solution = models.CharField(max_length=300)
    server = models.ForeignKey(Server, on_delete=models.CASCADE , null=True)
    def __str__(self):
        return self.type_of_incident
    
class IncidentStorageRack(models.Model):
    type_of_incident = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    date = models.DateField()
    solution = models.CharField(max_length=300)
    storage_rack = models.ForeignKey(StorageRack, on_delete=models.CASCADE , null=True)
    def __str__(self):
        return self.type_of_incident