import io
from typing import IO
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render
from django.http import FileResponse, HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from matplotlib import colors
from xhtml2pdf import pisa
from io import BytesIO
from inventaire.models import Server, StorageRack
from django.core.mail import EmailMessage
from .forms import EmailForm
import matplotlib.pyplot as plt
from io import BytesIO
from django.views import View
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from reportlab.pdfgen import canvas
from prometheus_client import Summary


reporting_view_execution_time = Summary('reporting_view_execution_time', 'Time spent processing reporting view')
@permission_required('auth.group_admin')
def inventory_report(request):
    servers = Server.objects.all()
    storage_racks = StorageRack.objects.all()
     
    template = get_template('reporting/inventory_report.html')
    context = {
        'servers': servers,
        'storage_racks': storage_racks
    }
    html = template.render(context)

    # Generate the PDF file
    pdf_file = BytesIO()
    pisa.CreatePDF(html, pdf_file)

    # Create a response object with the PDF file
    response = HttpResponse(pdf_file.getvalue(), content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="inventory_report.pdf"'

    return response

@permission_required('auth.group_admin')
def email_report(request):
    form = EmailForm(request.POST or None)
    
    if request.method == 'POST':
        if form.is_valid():
            # Generate the PDF report
            servers = Server.objects.all()
            storage_racks = StorageRack.objects.all()

            template = get_template('reporting/inventory_report.html')
            context = {
                'servers': servers,
                'storage_racks': storage_racks
            }
            html = template.render(context)

            # Generate the PDF file
            pdf_file = BytesIO()
            pisa.CreatePDF(html, pdf_file)

            # Create an email message with the PDF file attached
            email = EmailMessage(
                subject='Inventory Report',
                body='Please find attached the latest inventory report.',
                from_email=settings.EMAIL_HOST_USER,
                to=[form.cleaned_data['to_email']],
            )
            email.attach('inventory_report.pdf', pdf_file.getvalue(), 'application/pdf')
            email.send()

            return HttpResponse('Report sent successfully!')

    context = {
        'form': form
    }

    return render(request, 'reporting/email_report.html', context)

class ServerGraphPDFView(View):
    def get(self, request):
        # Get data from Server model
        servers = Server.objects.all()
        servers = sorted(servers, key=lambda server: server.ram)

        # Create graphs
        fig1, ax1 = plt.subplots(figsize=(10, 6))
        ax1.bar([server.serial_number for server in servers], [server.ram for server in servers])
        ax1.set_xlabel('serial_number' )
        ax1.set_ylabel('Mémoire vive')
        ax1.set_title('Utilisation de la RAM par Serveur')
        plt.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=0.1, hspace=0.2)

        servers = sorted(servers, key=lambda server: server.disk)
        fig2, ax2 = plt.subplots(figsize=(10, 6))
        ax2.bar([server.serial_number for server in servers], [server.disk for server in servers])
        ax2.set_xlabel('Numero de serie')
        ax2.set_ylabel('capacité du disque')
        ax2.set_title('Utilisation du Disk par Serveur')
        plt.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=0.1, hspace=0.2)

        servers = sorted(servers, key=lambda server: server.power)
        fig3, ax3 = plt.subplots(figsize=(10, 6))
        ax3.bar([server.serial_number for server in servers], [server.power for server in servers])
        ax3.set_xlabel('Numero de serie')
        ax3.set_ylabel('Puissance de consommation')
        ax3.set_title('Utilisation de la Puissance par Serveur')
        plt.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=0.1, hspace=0.2)

        # Generate PDF
        buffer = io.BytesIO()
        pdf_pages = PdfPages(buffer)
        pdf_pages.savefig(fig1)
        pdf_pages.savefig(fig2)
        pdf_pages.savefig(fig3)
        pdf_pages.close()

        # Create response
        buffer.seek(0)
        response = FileResponse(buffer, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="server_graphs.pdf"'
        return response
    
def indexreport(request):
    servers = Server.objects.all()
    storage_racks = StorageRack.objects.all()
    return render(request, 'reporting/inventory_report.html', {'servers': servers, 'storage_racks': storage_racks})