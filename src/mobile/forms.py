from django import forms

from .models import Category, Mobile, MobileImage

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('title', 'description')
        labels = {
            'title': 'Titre',
            'description': 'Description',
        }
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
        }

class MobileForm(forms.ModelForm):
    class Meta:
        model = Mobile
        fields = ('title', 'price','rom','ram','cpu','taille','resolution','profndeur','Rafra','system','battery','type','camera_front','camera_rear','video','category')
        labels = {
            'title': 'Titre du Mobile',
            'price': 'Prix',
            'rom': 'Stockage interne',
            'ram': 'Mémoire vive',
            'cpu': 'Processeur',
            'taille': 'Taille de l\'ecran',
            'resolution': 'Resolution de l\'ecran',
            'profndeur': 'Profondeur de Couleur',
            'Rafra': 'Rafraichissement de l\'ecran',
            'system': 'Système d\'exploitation',
            'battery': 'Batterie',
            'type': 'Type de Carte SIM',
            'camera_front': 'Camera Frontale',
            'camera_rear': 'Camera Arriere',
            'video': 'Resolution de la video',
            'category': 'Categorie',
        }
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'price': forms.NumberInput(attrs={'class': 'form-control'}),
            'rom': forms.NumberInput(attrs={'class': 'form-control'}),
            'ram': forms.NumberInput(attrs={'class': 'form-control'}),
            'cpu': forms.TextInput(attrs={'class': 'form-control'}),
            'taille': forms.NumberInput(attrs={'class': 'form-control'}),
            'resolution': forms.TextInput(attrs={'class': 'form-control'}),
            'profndeur': forms.NumberInput(attrs={'class': 'form-control'}),
            'Rafra': forms.NumberInput(attrs={'class': 'form-control'}),
            'system': forms.TextInput(attrs={'class': 'form-control'}),
            'battery': forms.NumberInput(attrs={'class': 'form-control'}),
            'type': forms.TextInput(attrs={'class': 'form-control'}),
            'camera_front': forms.NumberInput(attrs={'class': 'form-control'}),
            'camera_rear': forms.NumberInput(attrs={'class': 'form-control'}),
            'video': forms.TextInput(attrs={'class': 'form-control'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
        }

        error_messages = {
            'title': {
                'required': 'Le titre est obligatoire',
                'max_length': 'Le titre ne doit pas dépasser 100 caractères',
                'min_length': 'Le titre doit avoir au moins 3 caractères',
            },
            'price': {
                'required': 'Le prix est obligatoire',
            },
            'rom': {
                'required': 'Le stockage interne est obligatoire',
            },
            'ram': {
                'required': 'La mémoire vive est obligatoire',
            },
            'cpu': {
                'required': 'Le processeur est obligatoire',
            },
            'taille': {
                'required': 'La taille de l\'ecran est obligatoire',
            },
            'resolution': {
                'required': 'La resolution de l\'ecran est obligatoire',
            },
            'profndeur': {
                'required': 'La profondeur de couleur est obligatoire',
            },
            'Rafra': {
                'required': 'Le rafraichissement de l\'ecran est obligatoire',
            },
            'system': {
                'required': 'Le système d\'exploitation est obligatoire',
            },
            'battery': {
                'required': 'La batterie est obligatoire',
            },
            'type': {
                'required': 'Le type de carte SIM est obligatoire',
            },
            'camera_front': {
                'required': 'La camera frontale est obligatoire',
            },
            'camera_rear': {
                'required': 'La camera arriere est obligatoire',
            },
            'video': {
                'required': 'La resolution de la video est obligatoire',
            },
            'category': {
                'required': 'La categorie est obligatoire',
            },
        }

        

class MobileImageForm(forms.ModelForm):
    class Meta:
        model = MobileImage
        fields = ('mobile', 'image')
        labels = {
            'mobile': 'Mobile',
            'image': 'Image',
        }
        widgets = {
            'mobile': forms.Select(attrs={'class': 'form-control'}),
            'image': forms.FileInput(attrs={'class': 'form-control-file'}),
        }
