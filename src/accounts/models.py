from django.db import models
from django.contrib.auth.models import AbstractUser,Group
# Create your models here.


class Customer(AbstractUser):
    pass

class LoginLog(models.Model):
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    login_time = models.DateTimeField(auto_now_add=True)
    logout_time = models.DateTimeField(null=True, blank=True)
    status = models.BooleanField(default=True)
