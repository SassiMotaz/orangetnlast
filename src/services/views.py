from django.shortcuts import render
from django.views.generic import ListView ,DetailView, CreateView , UpdateView , DeleteView
from django.urls import reverse_lazy
from prometheus_client import Summary
from mobile.models import Mobile
from .models import Service , CategoryService
from .forms import ServiceForm , CategoryServiceForm
from django.contrib.auth.decorators import login_required , user_passes_test 
from django.utils.decorators import method_decorator
from django.contrib.auth.models import Group

# Create your views services here.
services_view_execution_time = Summary('inventaire_view_execution_time', 'Time spent processing inventaire view')
def home(request):
    return render(request, 'homeapp.html')

def index(request):
    return render(request, 'indexserv.html')

class HomeView(ListView):
    model = Service
    template_name = 'services\home.html'
    context_object_name = 'services'
    ordering = ['-id']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        categories = CategoryService.objects.all()
        context['title'] = 'Home'
        context['categories'] = categories
        return context

class ServiceDetailView(DetailView):
    model = Service
    template_name = 'services\detail.html'
    context_object_name = 'service'
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detail'
        mobile_services = self.object.mobiles.all()
        context['mobile_services'] = mobile_services
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['IT','Admin']).exists() or u.is_superuser), name='dispatch')
class ServiceCreateView(CreateView):
    model = Service
    form_class = ServiceForm
    template_name = 'services\\create.html'
    success_url = reverse_lazy('services:home')

    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Create'
        return context
    
    def form_valid(self, form):
        service = form.save()
        CategoryService = form.cleaned_data['category']
        CategoryService.services.add(service)
        service.save()
        return super().form_valid(form)

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['IT','Admin']).exists() or u.is_superuser), name='dispatch')
class ServiceUpdateView(UpdateView):
    model = Service
    template_name = 'services\\update.html'
    form_class = ServiceForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Update'
        return context
    

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['IT','Admin']).exists() or u.is_superuser), name='dispatch')
class ServiceDeleteView(DeleteView):
    model = Service
    template_name = 'services\\delete.html'
    success_url = reverse_lazy('services:home')
    context_object_name = 'service'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete'
        return context


# Create your views categories here.


class CategoryServiceListView(ListView):
    model = CategoryService
    template_name = 'catégories\categoryservice_list.html'
    context_object_name = 'categoryservices'
    ordering = ['id']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'CategoryService'
        return context
    
class CategoryServiceDetailView(DetailView):
    model = CategoryService
    template_name = 'catégories\categoryservice_detail.html'
    context_object_name = 'categoryservice'
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detail'
        return context

class ServicesCategoryListView(ListView):
    model = Service
    template_name = 'catégories\services_category_list.html'
    context_object_name = 'services'
    ordering = ['id']

    def get_queryset(self):
        return Service.objects.filter(category=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = CategoryService.objects.get(pk=self.kwargs['pk'])
        context['title'] = 'Services'
        context['category'] = category
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['IT','Admin']).exists() or u.is_superuser), name='dispatch')
class CategoryServiceCreateView(CreateView):
    model = CategoryService
    template_name = 'catégories\categoryservice_create.html'
    form_class = CategoryServiceForm
    success_url = reverse_lazy('services:category')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Create'
        return context
@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['IT','Admin']).exists() or u.is_superuser), name='dispatch') 
class CategoryServiceUpdateView(UpdateView):
    model = CategoryService
    template_name = 'catégories\categoryservice_update.html'
    form_class = CategoryServiceForm
    success_url = reverse_lazy('services:category')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Update'
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['IT','Admin']).exists() or u.is_superuser), name='dispatch')
class CategoryServiceDeleteView(DeleteView):
    model = CategoryService
    template_name = 'catégories\categoryservice_delete.html'
    success_url = reverse_lazy('services:category')
    context_object_name = 'categoryservice'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete'
        return context