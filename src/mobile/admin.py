from django.contrib import admin

# Register your models here.

from .models import Category, Mobile, MobileImage

admin.site.register(Category)
admin.site.register(Mobile)
admin.site.register(MobileImage)
