
from django.urls import path

from .views import ServerView , ServerDetailView , ServerCreateView , ServerUpdateView , ServerDeleteView, chart_form, generate_chart, previous_page, storagerack_chart, storagerack_form
from .views import export_servers_to_excel , import_servers_from_excel  
from .views import StorageRackView , StorageRackDetailView , StorageRackCreateView , StorageRackUpdateView , StorageRackDeleteView
from .views import export_storageracks_to_excel , import_storageracks_from_excel , index , indexserver , indexstoragerack
from .views import IncidentServerListView , IncidentServerCreateView , IncidentServerUpdateView , IncidentServerDeleteView , IncidentServerDetailView , indexincident
from .views import IncidentStorageRackCreateView , IncidentStorageRackUpdateView , IncidentStorageRackDeleteView , IncidentStorageRackDetailView , IncidentStorageRackListView
from .views import IncidentServerCreateView , IncidentServerUpdateView , IncidentServerDeleteView 
from .views import IncidentStorageRackCreateView , IncidentStorageRackUpdateView , IncidentStorageRackDeleteView 

app_name="inventaire"

urlpatterns = [
    #reverse path 
    path('previous/', previous_page, name='previous'),
    # list urls for server
    path ('', index, name='index'),
    path('server/', indexserver, name='gestionserver'),
    path('server/list', ServerView.as_view(), name='server'),
    path('server/<str:pk>', ServerDetailView.as_view(), name='server_detail'),
    path('server/create/', ServerCreateView.as_view(), name='server_create'),
    path('server/update/<str:pk>', ServerUpdateView.as_view(), name='server_update'),
    path('server/delete/<str:pk>', ServerDeleteView.as_view(), name='server_delete'),
    path('server/export/', export_servers_to_excel, name='exportserver'),
    path('server/import/', import_servers_from_excel, name='importserver'),
    path('server/stats/chart/', chart_form, name='chart_form'),
    path('server/stats/chart/generate/', generate_chart, name='generate_chart'),
    
    # list urls for storage rack
    path('storagerack/', indexstoragerack, name='gestionstoragerack'),
    path('storagerack/list', StorageRackView.as_view(), name='storagerack'),
    path('storagerack/<str:pk>', StorageRackDetailView.as_view(), name='storagerack_detail'),
    path('storagerack/create/', StorageRackCreateView.as_view(), name='storagerack_create'),
    path('storagerack/update/<str:pk>', StorageRackUpdateView.as_view(), name='storagerack_update'),
    path('storagerack/delete/<str:pk>', StorageRackDeleteView.as_view(), name='storagerack_delete'),
    path('storagerack/export/', export_storageracks_to_excel, name='export'),
    path('storagerack/import/', import_storageracks_from_excel, name='import'),
    path('storagerack/stats/chart', storagerack_form, name="storagerack_form"),
    path('storagerack/stats/chart/generate', storagerack_chart, name="storagerack_chart"), 


    path('incident/', indexincident, name='gestionincident'),

    # list urs for incident server
    path('incidentserver/list', IncidentServerListView.as_view(), name='incidentserver'),
    path('incidentserver/<str:pk>', IncidentServerDetailView.as_view(), name='incidentserver_detail'),
    path('incidentserver/create/', IncidentServerCreateView.as_view(), name='incidentserver_create'),
    path('incidentserver/update/<str:pk>', IncidentServerUpdateView.as_view(), name='incidentserver_update'),
    path('incidentserver/delete/<str:pk>', IncidentServerDeleteView.as_view(), name='incidentserver_delete'),
    
    # list urs for incident storagerack
    path('incidentstoragerack/list', IncidentStorageRackListView.as_view(), name='incidentstoragerack'),
    path('incidentstoragerack/<str:pk>', IncidentStorageRackDetailView.as_view(), name='incidentstoragerack_detail'),
    path('incidentstoragerack/create/', IncidentStorageRackCreateView.as_view(), name='incidentstoragerack_create'),
    path('incidentstoragerack/update/<str:pk>', IncidentStorageRackUpdateView.as_view(), name='incidentstoragerack_update'),
    path('incidentstoragerack/delete/<str:pk>', IncidentStorageRackDeleteView.as_view(), name='incidentstoragerack_delete'),
    #path('incidentstoragerack/<str:pk>', IncidentStorageRackDetailView.as_view(), name='incidentstoragerack_detail'),
    
]
