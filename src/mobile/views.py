
from .models import Category, Mobile, MobileImage
from django.views.generic import ListView ,DetailView, CreateView , UpdateView , DeleteView
from .forms import CategoryForm, MobileForm
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from prometheus_client import Summary
from prometheus_client.core import CollectorRegistry

def index (request):
  return render(request, 'index.html')

class CategoryListView(ListView):
    model = Category
    template_name = 'category\category_list.html'
    context_object_name = 'categories'
    ordering = ['id']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Categories'
        return context

class CategoryDetailView(DetailView):
    model = Category
    template_name = 'category\category_detail.html'
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mobiles = Mobile.objects.filter(category=self.get_object())
        context['title'] = 'Category'
        context['mobiles'] = mobiles
        return context


class MobileCategoryListView(ListView):
    model = Mobile
    template_name = 'mobile\mobile_list_category.html'
    context_object_name = 'mobiles'
    ordering = ['-id']

    def get_queryset(self):
        return Mobile.objects.filter(category__id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = Category.objects.get(pk=self.kwargs['pk'])
        context['title'] = 'Mobiles'
        context['category'] = category
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['Admin','SMC','IT']).exists() or u.is_superuser), name='dispatch')
class CategoryCreateView(CreateView):
    model = Category
    template_name = 'category\category_create.html'
    form_class = CategoryForm
    success_url = reverse_lazy('mobile:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Create Category'
        return context
   
@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['Admin','SMC','IT']).exists() or u.is_superuser), name='dispatch')
class CategoryUpdateView(UpdateView):
    model = Category
    template_name = 'category\category_update.html'
    form_class = CategoryForm
    success_url = reverse_lazy('mobile:home')
    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Update Category'
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['Admin','SMC','IT']).exists() or u.is_superuser), name='dispatch')
class CategoryDeleteView(DeleteView):
    model = Category
    template_name = 'category\category_delete.html'
    context_object_name = 'category'
    success_url = reverse_lazy('mobile:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete Category'
        return context
    
# Mobile Views

class MobileListView(ListView):
    model = Mobile
    template_name = 'mobile\mobile_list.html'
    context_object_name = 'mobiles'
    ordering = ['-id']
    paginate_by = 1

    def get_context_data(self, **kwargs):
        categories = Category.objects.all()
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mobiles'
        context['categories'] = categories
        
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['Admin','SMC','IT']).exists() or u.is_superuser), name='dispatch')
class MobileCreateView(CreateView):
    model = Mobile
    template_name = 'mobile\mobile_create.html'

    form_class = MobileForm
    success_url = reverse_lazy('mobile:mobile_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = Category.objects.all()
        context['title'] = 'Create Mobile'
        context['category'] = category
        return context
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            image = request.FILES.get('image')
            image1 = request.FILES.get('image1')
            image2 = request.FILES.get('image2')     
            mobile = Mobile.objects.last()
            MobileImage.objects.create(mobile=mobile, image=image)
            MobileImage.objects.create(mobile=mobile, image=image1)
            MobileImage.objects.create(mobile=mobile, image=image2)
            Category.objects.get(id=request.POST.get('category')).mobiles.add(mobile)
            return redirect('mobile:mobile_list')
        else:
            return render(request, self.template_name, {'form':form })

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['Admin','SMC','IT']).exists() or u.is_superuser), name='dispatch')
class MobileUpdateView(UpdateView):
    model = Mobile
    template_name = 'mobile\mobile_update.html'
    form_class = MobileForm
    success_url = reverse_lazy('mobile:mobile_list')
   

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Update Mobile'
        return context

@method_decorator(user_passes_test(lambda u: u.groups.filter(name__in =['Admin','SMC','IT']).exists() or u.is_superuser), name='dispatch')
class  MobileDeleteView(DeleteView):
    model = Mobile
    template_name = 'mobile\mobile_delete.html'
    context_object_name = 'mobile'
    success_url = reverse_lazy('mobile:mobile_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete Mobile'
        return context
    
class MobileDetailView(DetailView):
    model = Mobile
    template_name = 'mobile\mobile_detail.html'
    context_object_name = 'mobile'
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        images = MobileImage.objects.filter(mobile=self.object)
        category = Mobile.objects.get(id=self.object.id)
        context['images'] = images
        context['title'] = 'Mobile'
        context['category'] = category
        return context