from django import forms

from .models import Service , CategoryService


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = ('title', 'content', 'image', 'pubished','Appels', 'SMS', 'Prix','mobiles','category')
        labels = {
            'title': 'Titre',
            'content': 'Contenu',
            'image': 'Image',
            'pubished': 'Publié',
            'Appels': 'Appels',
            'SMS': 'SMS',
            'Prix': 'Prix',
            'mobiles': 'Mobiles',
            'category': 'Catégorie',
        }
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'content': forms.Textarea(attrs={'class': 'form-control'}),
            'pubished': forms.CheckboxInput(attrs={'class': 'form-check-input mx-5'}),
            'image': forms.FileInput(attrs={'class': 'form-control-file'}),
            'Appels': forms.NumberInput(attrs={'class': 'form-control'}),
            'SMS': forms.NumberInput(attrs={'class': 'form-control'}),
            'Prix': forms.NumberInput(attrs={'class': 'form-control'}),
            'mobiles': forms.CheckboxSelectMultiple(attrs={'class': 'form-check-input' }),
            'category': forms.Select(attrs={'class': 'selectpicker form-control'}),
        }
  
class CategoryServiceForm(forms.ModelForm):
    class Meta:
        model = CategoryService
        fields = ('title', 'description')
        labels = {
            'title': 'Titre',
            'description': 'Description',
        }
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
        }