from django.contrib import admin
from .models import Service
from .models import CategoryService

# Register your models here.

class ServiceAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('title', 'subtitle', 'pubished', 'created' , 'updated','image', 'Appels', 'SMS', 'Prix')

    
admin.site.register(Service, ServiceAdmin)

admin.site.register(CategoryService)