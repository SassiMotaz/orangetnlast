# Generated by Django 4.2a1 on 2023-04-12 10:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventaire', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='server',
            name='ip_address',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
