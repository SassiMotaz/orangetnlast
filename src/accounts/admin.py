from django.contrib import admin

from accounts.models import Customer , LoginLog

# Register your models here.
admin.site.register(Customer)
admin.site.register(LoginLog)
