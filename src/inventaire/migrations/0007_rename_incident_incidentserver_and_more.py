# Generated by Django 4.2a1 on 2023-05-01 10:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventaire', '0006_rename_incidentserver_incident_and_more'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Incident',
            new_name='IncidentServer',
        ),
        migrations.RenameField(
            model_name='incidentserver',
            old_name='inventory',
            new_name='server',
        ),
        migrations.CreateModel(
            name='IncidentStorageRack',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_of_incident', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=50)),
                ('date', models.DateField()),
                ('solution', models.CharField(max_length=50)),
                ('storage_rack', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='inventaire.storagerack')),
            ],
        ),
    ]
